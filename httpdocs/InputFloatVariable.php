<?php

require_once __DIR__ . '/Variable.php';

/**
 * Class InputVariable
 */
class InputFloatVariable implements Variable
{

    /**
     * @var float
     */
    private $value;

    /**
     * InputVariable constructor.
     * @param string $externalValue
     */
    public function __construct($externalValue)
    {
        $this->setValue($externalValue);
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    public function abc()
    {

    }

    /**
     * @param string $externalValue
     */
    public function setValue($externalValue)
    {
        $this->value = (float)$externalValue;
    }
}