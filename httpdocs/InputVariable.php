<?php

require_once __DIR__ . '/Variable.php';

/**
 * Class InputVariable
 */
class InputVariable implements Variable
{

    /**
     * @var float
     */
    private $value;

    /**
     * InputVariable constructor.
     * @param string $externalValue
     */
    public function __construct($externalValue)
    {
        $this->setValue($externalValue);
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $externalValue
     */
    public function setValue($externalValue)
    {
        $this->value = (float)$externalValue;
    }
}