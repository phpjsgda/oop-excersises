<?php

class PhoneBuilder
{
    /**
     * @var string
     */
    protected $plug = '';
    /**
     * @var string
     */
    protected $signalConverter = '';
    /**
     * @var string
     */
    private $handset = '';
    /**
     * @var bool
     */
    private $bell = '';
    /**
     * @var string
     */
    private $bodyColor = '#FFFFFF';
    /**
     * @var Validator
     */
    private $validator;

    public function __construct()
    {
        $this->validator = new Validator();
    }

    /**
     * @param Validator $validator
     * @return PhoneBuilder
     */
    public function withValidator(Validator $validator)
    {
        $this->validator = $validator;
        return $this;
    }

    /**
     * @param string $color
     * @return PhoneBuilder
     */
    public function withBodyColor($color)
    {
        $this->bodyColor = $color;
        return $this;
    }

    /**
     * @param $handset
     * @return PhoneBuilder
     */
    public function withHandset($handset)
    {
        $this->handset = $handset;
        return $this;
    }

    public function build()
    {
        return new Phone(
            $this->plug,
            $this->signalConverter,
            $this->handset,
            $this->bell,
            $this->bodyColor,
            $this->validator
        );
    }

}