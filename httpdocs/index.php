<?php

require_once __DIR__ . '/Calculate.php';
require_once __DIR__ . '/InputVariable.php';
require_once __DIR__ . '/InputFloatVariable.php';
require_once __DIR__ . '/Validate.php';

$var2 = new InputVariable('aa');
$var1 = new InputFloatVariable('2');

$validator = new validate($var1, $var2);
 
$calculator = new Calculate($var1, $var2);

try{
    $validator->validateData();
    echo $calculator->calculate();
}catch (NotFloatVariableException $e){
 if(Validate::ERROR_IN_VAR_1 === $e->getCode()){
     echo $e->getMessage() . 'var1';
 }else{
     echo $e->getMessage() . 'var2';
 }

}
