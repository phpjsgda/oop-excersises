<?php

class Calculate
{
    /**
     * @var InputVariable
     */
    private $var1;
    /**
     * @var InputVariable
     */
    private $var2;

    /**
     * Calculate constructor.
     * @param Variable $var1
     * @param Variable $var2
     */
    public function __construct(Variable $var1, Variable $var2)
    {
        $this->var1 = $var1;
        $this->var2 = $var2;
    }

    public function  calculate()
    {
        $result = $this->var1->getValue() + $this->var2->getValue();

        return $result;
    }
}