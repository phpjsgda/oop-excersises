<?php

class Phone
{
    /**
     * @var string
     */
    protected $plug;
    /**
     * @var string
     */
    protected $signalConverter;
    /**
     * @var string
     */
    private $handset;
    /**
     * @var bool
     */
    private $bell;
    /**
     * @var string
     */
    private $bodyColor;
    /**
     * @var Validator
     */
    private $validator;

    public function __construct($plug, $signalConverter, $handset, $bell, $bodyColor = 'black', Validator $validator)
    {
        $this->plug = $plug;
        $this->signalConverter = $signalConverter;
        $this->handset = $handset;
        $this->bodyColor = $bodyColor;
        $this->bell = $bell;
        $this->validator = $validator;
    }

    /**
     * @return Validator
     */
    public function getValidator()
    {
        return $this->validator;
    }

    /**
     * @return string
     */
    public function pickUpHandset()
    {
        $this->signalConverter = $this->plug;
        $this->handset = $this->signalConverter;
        return $this->handset;
    }

    public function hangUp()
    {
        $this->finishCall()
        $this->handset = 'off';
    }

    /**
     * @param $signal
     */
    protected function startIncommingCall($signal)
    {
        $this->ringBell();
        $this->signalConverter = $this->plug . $signal;
    }

    protected function finishCall()
    {
        $this->stopBell();
        $this->signalConverter = 'off';
    }

    protected function ringBell()
    {
        $this->bell = true;
    }

    protected function stopBell()
    {
        $this->bell = false;
    }

    protected function establishConnection()
    {
        $this->signalConverter = $this->plug;
    }
}