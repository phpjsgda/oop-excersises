<?php

require_once __DIR__ . '/NotFloatVariableException.php';

class Validate {

    const ERROR_IN_VAR_1 = 12344;
    const ERROR_IN_VAR_2 = 2;
/*
*  @var InputVariable;
*/
    
private $var1;
private $var2;

public function __construct(Variable $var1, Variable $var2) {
    
    $this->var1 = $var1;
    $this->var2 = $var2;
}

public function validateData() {
    if (false === $this->validateVariable($this->var1->getValue())) {
        throw new NotFloatVariableException("wartośc nie jest liczba", self::ERROR_IN_VAR_1);
    }
    if (false === $this->validateVariable($this->var2->getValue())) {
        throw new NotFloatVariableException("wartośc nie jest liczba", self::ERROR_IN_VAR_2);
    }
}

private function validateVariable($data) {
    if (!is_numeric($data)) {
      
        return false;
    }

    return true;
}

}